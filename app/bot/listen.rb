require "facebook/messenger"
include Facebook::Messenger
Facebook::Messenger::Subscriptions.subscribe

Facebook::Messenger::Thread.set(
  setting_type: 'call_to_actions',
  thread_state: 'new_thread',
  call_to_actions: [
    {
      payload: 'WELCOME_USER'
    }
  ]
)

Facebook::Messenger::Thread.set(
  setting_type: 'greeting',
  greeting: {
    text: 'HI, I\'m Mr. Copyright and I\'m here to inform you about copyright evidence studies all across the world.'
  }
)

Facebook::Messenger::Thread.set(
  setting_type: 'call_to_actions',
  thread_state: 'existing_thread',
  call_to_actions: [
    {
      type: 'postback',
      title: 'Search study',
      payload: 'WELCOME_SEARCH'
    },
    {
      type: 'postback',
      title: 'Subscribe to a topic',
      payload: 'SUBSCRIBE_TOPIC'
    },
    {
      type: 'postback',
      title: 'Explore categories',
      payload: 'EXPLORE_CATEGORIES'
    }
  ]
)


Bot.on :postback do |postback|
  # postback.sender    # => { 'id' => '1008372609250235' }
  # postback.recipient # => { 'id' => '2015573629214912' }
  # postback.sent_at   # => 2016-04-22 21:30:36 +0200
  # postback.payload   # => 'EXTERMINATE'

  puts "POSTBACK ====>" + postback.inspect
  message = BotMessageBuilder.send(postback.payload, postback)

  Bot.deliver(recipient: postback.sender, message: message)
end

Bot.on :message do |message|
  # message.id          # => 'mid.1457764197618:41d102a3e1ae206a38'
  # message.sender      # => { 'id' => '1008372609250235' }
  # message.seq         # => 73
  # message.sent_at     # => 2016-04-22 21:30:36 +0200
  # message.text        # => 'Hello, bot!'
  # message.attachments # => [ { 'type' => 'image', 'payload' => { 'url' => 'https://www.example.com/1.jpg' } } ]
  if message.quick_reply.present?
    message_built = BotMessageBuilder.send(message.quick_reply, message)
  elsif message.text.nil?
    message_built = BotMessageBuilder.send("INVALID_MESSAGE", message)
  else
    message_built = BotMessageBuilder.send("SEARCH_STUDY__#{message.text}", message)
  end

  Bot.deliver(recipient: message.sender, message: message_built)

end




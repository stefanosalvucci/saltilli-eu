module BotMessageBuilder

  CATEGORIES = {
    'Advertising' => {
        title: 'Advertising',
        payload: 'SHOW_CATEGORY__Advertising',
        image: 'http://www.online-inzerce.eu/wp-content/uploads/2011/10/funny-advertisements.jpg',
        subtitle: '4 studies',
        wiki_url: 'http://www.copyrightevidence.org/evidence-wiki/index.php/Advertising'
      },
    'Architectural' => {
        title: 'Architectural',
        payload: 'SHOW_CATEGORY__Architectural',
        image: 'http://digitalfrontierblog.com/wp-content/uploads/2015/04/theater-sydney-opera-house.jpg',
        subtitle: '0 studies',
        wiki_url: 'http://www.copyrightevidence.org/evidence-wiki/index.php/Architectural'
      },
    'Computer consultancy' => {
        title: 'Computer consultancy',
        payload: 'SHOW_CATEGORY__Computer consultancy',
        image: 'http://www.zealwebmechanics.com/wp-content/uploads/2015/10/24370896_l-200x200.jpg',
        subtitle: '7 studies',
        wiki_url: 'http://www.copyrightevidence.org/evidence-wiki/index.php/Computer_consultancy'
      },
    'Computer programming' => {
        title: 'Computer programming',
        payload: 'SHOW_CATEGORY__Computer programming',
        image: 'https://qph.ec.quoracdn.net/main-thumb-t-244761-200-hwrghehfappmouinxougzanrpbrskeez.jpeg',
        subtitle: '31 studies',
        wiki_url: 'http://www.copyrightevidence.org/evidence-wiki/index.php/Computer_programming'
      },
    'Creative, arts and entertainment' => {
        title: 'Creative, arts and entertainment',
        payload: 'SHOW_CATEGORY__Creative, arts and entertainment',
        image: 'http://rs687.pbsrc.com/albums/vv233/madebyfridah/Freddie%20Mercury/FreddieMercury11_zpseb1ca058.jpg~c200',
        subtitle: '196 studies',
        wiki_url: 'http://www.copyrightevidence.org/evidence-wiki/index.php/Creative,_arts_and_entertainment'
      },
    'Cultural education' => {
        title: 'Cultural education',
        payload: 'SHOW_CATEGORY__Cultural education',
        image: 'http://culture-routes.net/sites/default/files/images/menu-bg/cultural-routes-countries.jpg',
        subtitle: '72 studies',
        wiki_url: 'http://www.copyrightevidence.org/evidence-wiki/index.php/Cultural_education'
      },
    'Film and motion pictures' => {
        title: 'Film and motion pictures',
        payload: 'SHOW_CATEGORY__Film and motion pictures',
        image: 'http://rs719.pbsrc.com/albums/ww200/keithsweeet/pulp_fiction_108_41_de.jpg~c200',
        subtitle: '161 studies',
        wiki_url: 'http://www.copyrightevidence.org/evidence-wiki/index.php/Film_and_motion_pictures'
      },
    'PR and communication' => {
        title: 'PR and communication',
        payload: 'SHOW_CATEGORY__PR and communication',
        image: 'http://www.russia-direct.org/sites/default/files/field/image/AP_905118210502-russia-direct-us-russia-relations-obama-putin-talks-200.jpg',
        subtitle: '3 studies',
        wiki_url: 'http://www.copyrightevidence.org/evidence-wiki/index.php/PR_and_communication'
      },
    'Photographic activities' => {
        title: 'Photographic activities',
        payload: 'SHOW_CATEGORY__Photographic activities',
        image: 'https://qph.ec.quoracdn.net/main-thumb-t-1277-200-vsdniciqwclgdrkvigrfaedrbhicnlgw.jpeg',
        subtitle: '21 studies',
        wiki_url: 'http://www.copyrightevidence.org/evidence-wiki/index.php/Photographic_activities'
      },
    'Programming and broadcasting' => {
        title: 'Programming and broadcasting',
        payload: 'SHOW_CATEGORY__Programming and broadcasting',
        image: 'http://www.thefeng.org/mattsblog/wp-content/uploads/2016/06/bigstock-Link-Sharing-Information-Techn-134102939-200x200.jpg',
        subtitle: '34 studies',
        wiki_url: ''
      },
    'Publishing of books, periodicals and other publishing' => {
        title: 'Publishing of books, periodicals and other publishing',
        payload: 'SHOW_CATEGORY__Publishing of books, periodicals and other publishing',
        image: 'http://www.coastalmags.com/downloads/2116/download/books.jpg?cb=724b9aaf48f643540283c1a630511a7c&w=200&ar=',
        subtitle: '138 studies',
        wiki_url: 'http://www.copyrightevidence.org/evidence-wiki/index.php/Programming_and_broadcasting'
      },
    'Software publishing (including video games)' => {
        title: 'Software publishing (including video games)',
        payload: 'SHOW_CATEGORY__Software publishing (including video games)',
        image: 'http://imgusr.tradekey.com/o-B2275055-20090316034236/wholesale-software-new-microsoft-windows-xp-home-edition-oem-sp3.jpg',
        subtitle: '185 studies',
        wiki_url: 'http://www.copyrightevidence.org/evidence-wiki/index.php/Software_publishing_(including_video_games)'
      },
    'Sound recording and music publishing' => {
        title: 'Sound recording and music publishing',
        payload: 'SHOW_CATEGORY__Sound recording and music publishing',
        image: 'https://d30y9cdsu7xlg0.cloudfront.net/png/89710-200.png',
        subtitle: '275 studies',
        wiki_url: 'http://www.copyrightevidence.org/evidence-wiki/index.php/Sound_recording_and_music_publishing'
      },
    'Specialised design' => {
        title: 'Specialised design',
        payload: 'SHOW_CATEGORY__Specialised design',
        image: 'http://wayupdesign.com/images/graphic-design.png',
        subtitle: '9 studies',
        wiki_url: 'http://www.copyrightevidence.org/evidence-wiki/index.php/Specialised_design'
      },
    'Television programmes' => {
        title: 'Television programmes',
        payload: 'SHOW_CATEGORY__Television programmes',
        image: 'http://www.nerodigitale.it/3544445-home_default/LG-24LF450B-TV-LED-24.jpg',
        subtitle: '75 studies',
        wiki_url: 'http://www.copyrightevidence.org/evidence-wiki/index.php/Television_programmes'
      },
    'Translation and interpretation' => {
        title: 'Translation and interpretation',
        payload: 'SHOW_CATEGORY__Translation and interpretation',
        image: 'http://i-cdn.phonearena.com/images/article/76717-image/Google-will-soon-be-able-to-translate-message-text-within-any-app.jpg',
        subtitle: '5 studies',
        wiki_url: 'http://www.copyrightevidence.org/evidence-wiki/index.php/Translation_and_interpretation'
    }
  }


  def self.MOCK_THE_PITCH_WITH_A_FANTASTIC_LIVE_NOTIFICATION
    Bot.deliver(
      recipient: {
        id: '1002651599862135'
      },
      message: {
        text: 'Great news! 2 new studies for you favourites topics are out.'
      },
      access_token: ENV['FB_ACCESS_TOKEN']
    )

    category = Sector.find_by_name('Sound recording and music publishing')
    studies = category.studies.last(2).map{|s| study_element_builder(s)}
    Bot.deliver(
      recipient: {
        id: '1002651599862135'
      },
      message:  cover_list_builder([cover_category_element_builder(category.name), studies].flatten, nil, nil),
      access_token: ENV['FB_ACCESS_TOKEN']
    )

  end

  def self.method_missing(m, *args, &block)
    puts "called POSTBACK #{m.to_s}"
    if m.to_s.starts_with?('SHOW_CATEGORY')
      _, category, _, offset = m.to_s.split('__')
      return SHOW_CATEGORY(category, offset.to_i)
    end
    if m.to_s.starts_with?('EXPLORE_CATEGORIES')
      _,  offset = m.to_s.split('__')
      return EXPLORE_CATEGORIES_ITERATOR(offset.to_i)
    end
    if m.to_s.starts_with?('SHOW_STUDY')
      _,  study = m.to_s.split('__')
      puts "calling SHOW_STUDY with study: #{study}"
      return SHOW_STUDY(study)
    end
    if m.to_s.starts_with?('SHOW_AUTHORS')
      _, study, _, offset = m.to_s.split('__')
      puts "calling SHOW_AUTHORS with study: #{study}"
      return SHOW_AUTHORS(study, offset.to_i)
    end
    if m.to_s.starts_with?('EXPLORE_AUTHOR')
      _, author, _, offset = m.to_s.split('__')
      puts "calling EXPLORE AUTHOR with author: #{author}"
      return EXPLORE_AUTHOR(author, offset.to_i)
    end
    if m.to_s.starts_with?('STUDY_CATEGORIES')
      _, study, _, offset = m.to_s.split('__')
      puts "calling STUDY_CATEGORIES with study: #{study}"
      return STUDY_CATEGORIES(study, offset.to_i)
    end
    if m.to_s.starts_with?('SEARCH_STUDY')
      _, keyword, _, offset = m.to_s.split('__')
      puts "calling SEARCH with keyword: #{keyword}"
      return SEARCH(keyword, offset.to_i)
    end
    if m.to_s.starts_with?('TOPIC_SUBSCRIPTION')
      _, topic = m.to_s.split('__')
      puts "calling TOPIC_SUBSCRIPTION with topic: #{topic}"
      return TOPIC_SUBSCRIPTION(topic)
    end
    {
      attachment:{
        type:"template",
        payload:{
          template_type:"generic",
          elements:[
            {
              title:"Ouch, my developers are gone.",
              image_url:"http://static4.businessinsider.com/image/56024ae6dd089578138b461f-480/a-resident-drinks-whiskey-from-the-bottle-while-in-the-sea-at-a-beach-in-boca-chica-may-17-2010.jpg",
              subtitle:"Too much of anything is bad, but too much good whiskey is barely enough."
            }
          ]
        }
      }
    }
  end


  def self.SEARCH keyword, offset
    offset = 0 if offset.nil?
    studies = Study.where('title LIKE ? OR description LIKE ?', "%#{keyword}%", "%#{keyword}%")
    studies_count = studies.count
    studies = studies[offset, 3].map{|s| study_element_builder(s)}
    if studies.count.zero?
      no_result_message_builder(keyword)
    elsif offset <  Study.where('title LIKE ? OR description LIKE ?', "%#{keyword}%", "%#{keyword}%").count - 3
      cover_list_builder([cover_search_element_builder(keyword, studies_count), studies].flatten, 'More studies', "SEARCH_STUDY__#{keyword}__OFFSET__#{offset + 3}")
    else
      cover_list_builder([cover_search_element_builder(keyword, studies_count), studies].flatten, nil, nil)
    end
  end


  def self.WELCOME_SEARCH postback
    {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [
            {
              title: 'I\'m here to serve you. Type something and I\'ll do a research for you.',
              image_url: 'http://www.chroniquedisney.fr/imgPerso/mechant/1970-edgar-02.jpg'
            }
          ]
        }
      }
    }
  end

  def self.SUBSCRIBE_TOPIC postback
    categories = []

    Sector.most_updated.each do |s|
      categories << {
        content_type:"text",
        title: s.name.truncate(20),
        payload:"TOPIC_SUBSCRIPTION__#{s.name}"
      }
    end

    {
        text: "Pick a topic:",
        quick_replies: categories
      }
  end


  def self.WELCOME_USER postback
    {
      attachment: {
        type: 'template',
        payload: {
          template_type: 'button',
          text: 'Hello, how can I help you ?',
          buttons: [
            { type: 'postback', title: 'Search study', payload: 'WELCOME_SEARCH' },
            { type: 'postback', title: 'Subscribe to a topic', payload: 'SUBSCRIBE_TOPIC' },
            { type: 'postback', title: 'Explore categories', payload: 'EXPLORE_CATEGORIES' }
          ]
        }
      }
    }
  end

  def self.SHOW_AUTHORS study, offset
    offset = 0 if offset.nil?
    authors = Study.find_by_title(study).authors[offset, 3].map{|a| author_element_builder(a)}
    if offset < Study.find_by_title(study).authors.count - 3
      cover_list_builder([cover_study_element_builder(study), authors].flatten, 'More authors', "SHOW_AUTHORS__#{study}__OFFSET__#{offset + 3}")
    else
      cover_list_builder([cover_study_element_builder(study), authors].flatten, nil, nil)
    end
  end

  def self.EXPLORE_AUTHOR author, offset
    offset = 0 if offset.nil?
    studies = Author.find_by_name(author).studies[offset, 3].map{|s| study_element_builder(s)}
    if offset < Author.find_by_name(author).studies.count - 3
      cover_list_builder([cover_author_element_builder(author), studies].flatten, 'More studies', "EXPLORE_AUTHOR__#{author}__OFFSET__#{offset + 3}")
    else
      cover_list_builder([cover_author_element_builder(author), studies].flatten, nil, nil)
    end
  end

  def self.STUDY_CATEGORIES study, offset
    offset = 0 if offset.nil?
    categories = Study.find_by_title(study).sectors[offset, 3].map{|s| category_element_builder(s.name)}
    if offset < Study.find_by_title(study).sectors.count - 3
      cover_list_builder([cover_study_element_builder(study), categories].flatten, 'More categories', "STUDY_CATEGORIES__#{study}__OFFSET__#{offset + 3}")
    else
      cover_list_builder([cover_study_element_builder(study), categories].flatten, nil, nil)
    end
  end

  def self.SHOW_STUDY study
    study_detail_builder(Study.find_by_title(study))
  end

  def self.SHOW_CATEGORY category, offset
    offset = 0 if offset.nil?
    studies = Sector.find_by_name(category).studies[offset, 3].map{|s| study_element_builder(s)}
    if offset < Sector.find_by_name(category).studies.count - 3
      cover_list_builder([cover_category_element_builder(category), studies].flatten, 'Show more', "SHOW_CATEGORY__#{category}__OFFSET__#{offset + 3}")
    else
      cover_list_builder([cover_category_element_builder(category), studies].flatten, nil, nil)
    end
  end

  def self.EXPLORE_CATEGORIES_ITERATOR offset
    offset = 0 if offset.nil?
    categories = Sector.all[offset, 4].map{|c| category_element_builder(c.name)}
    if offset < Sector.all.count - 4
      list_builder(categories, 'Show more', "EXPLORE_CATEGORIES_OFFSET__#{offset + 4}")
    else
      list_builder(categories, nil, nil)
    end
  end

  def self.list_builder elements, button_text, button_payload
    button = []
    if button_text.present? && button_payload.present?
      button = [{
        title: button_text,
        type: "postback",
        payload: button_payload
      }]
    end
    {
      attachment: {
        type: "template",
        payload: {
          template_type: "list",
          top_element_style: "compact",
          elements: elements,
          buttons: button
        }
      }
    }
  end


  def self.cover_list_builder elements, button_text, button_payload
    button = []
    if button_text.present? && button_payload.present?
      button = [{
        title: button_text,
        type: "postback",
        payload: button_payload
      }]
    end
    {
      attachment: {
        type: "template",
        payload: {
          template_type: "list",
          elements: elements,
          buttons: button
        }
      }
    }
  end


  def self.category_element_builder category_name
    category = Sector.find_by_name category_name
    {
      title: category.name,
      image_url: CATEGORIES[category.name][:image],
      subtitle: "#{category.studies.count} studies",
      buttons: [
        {
          title: 'Show studies',
          type: 'postback',
          payload: "SHOW_CATEGORY__#{category.name}"
        }
      ]
    }
  end

  def self.study_detail_builder study
    {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [
            {
              title: study.title.force_encoding("UTF-8"),
              item_url: study.url.force_encoding("UTF-8"),
              subtitle: "#{study.year} - #{study.countries.map(&:value).join(', ')}",
              buttons: [
                {
                  type: "postback",
                  title: 'Show authors',
                  payload: "SHOW_AUTHORS__#{study.title}"
                },
                {
                  type: "postback",
                  title: 'Show categories',
                  payload: "STUDY_CATEGORIES__#{study.title}"
                },
                {
                  "type":"element_share"
                }
              ]
            }
          ]
        }
      }
    }
  end

  def self.study_element_builder study
    {
      title: study.title.force_encoding("UTF-8"),
      subtitle: study.description.force_encoding("UTF-8"),
      buttons: [
        {
          title: 'More',
          type: 'postback',
          payload: "SHOW_STUDY__#{study.title}"
        }
      ]
    }
  end

  def self.author_element_builder author
    {
      title: author.name,
      subtitle: "#{author.studies.count} studies",
      buttons: [
        {
          title: 'Show studies',
          type: 'postback',
          payload: "EXPLORE_AUTHOR__#{author.name}"
        }
      ]
    }
  end

  def self.cover_author_element_builder author
    author = Author.find_by_name(author)
    {
      title: author.name,
      image_url: 'http://www.londonbookfair.co.uk/RXUK/RXUK_LondonBookFair/LBF%202015/Images/Author%20680%20x%20300.jpg?v=635599704170356154',
      subtitle: "#{author.studies.count} studies",
      buttons: [
        {
          title: 'Go to Wiki',
          type: 'web_url',
          url: author.url
        }
      ]
    }
  end

  def self.cover_search_element_builder keyword, results_count
    {
      title: "Searching for: #{keyword}",
      image_url: 'http://searchengineland.com/figz/wp-content/seloads/2014/08/search-engines-all-category-ss-1920-800x450.jpg',
      subtitle: "#{results_count} results"
    }
  end

  def self.cover_study_element_builder study
    study = Study.find_by_title(study)
    {
      title: study.title.force_encoding("UTF-8"),
      image_url: CATEGORIES[study.sectors.first.name][:image],
      subtitle: "#{study.year} - #{study.countries.map(&:value).join(', ')}",
      buttons: [
        {
          title: 'Go to Wiki',
          type: 'web_url',
          url: study.url
        }
      ]
    }
  end

  def self.cover_category_element_builder category_name
    {
      title: CATEGORIES[category_name][:title],
      image_url: CATEGORIES[category_name][:image],
      subtitle: CATEGORIES[category_name][:subtitle],
      buttons: [
        {
          title: 'Go to Wiki',
          type: 'web_url',
          url: CATEGORIES[category_name][:wiki_url]
        }
      ]
    }
  end

  def self.no_result_message_builder keyword
    {
      attachment:{
        type:"template",
        payload:{
          template_type:"generic",
          elements:[
            {
              title:"Sorry, no match for \"#{keyword}\".",
              image_url:"http://static4.businessinsider.com/image/56024ae6dd089578138b461f-480/a-resident-drinks-whiskey-from-the-bottle-while-in-the-sea-at-a-beach-in-boca-chica-may-17-2010.jpg",
              subtitle:"Too much of anything is bad, but too much good whiskey is barely enough."
            }
          ]
        }
      }
    }
  end


  def self.INVALID_MESSAGE message
    {
      attachment:{
        type:"template",
        payload:{
          template_type:"generic",
          elements:[
            {
              title:"Sorry, I'm not ready for that.",
              image_url:"https://cdn.pixabay.com/photo/2015/12/06/20/31/frog-1079978_960_720.jpg",
              subtitle: "I'm relaxing on a sofa ;)"
            }
          ]
        }
      }
    }
  end

  def self.TOPIC_SUBSCRIPTION topic
    topic = Sector.where('name LIKE ?', "#{topic}%").first
    {
      attachment:{
        type:"template",
        payload:{
          template_type:"generic",
          elements:[
            {
              title:"Great! We\'ll keep you updated about #{topic.name}",
              image_url:"https://d262ilb51hltx0.cloudfront.net/max/712/1*c3cQvYJrVezv_Az0CoDcbA.jpeg"
            }
          ]
        }
      }
    }
  end

end
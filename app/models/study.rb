class Study < ActiveRecord::Base
  has_and_belongs_to_many :countries
  has_and_belongs_to_many :sectors
  has_and_belongs_to_many :authors

end

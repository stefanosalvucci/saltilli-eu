class Sector < ActiveRecord::Base
  has_and_belongs_to_many :studies

  scope :most_updated, ->{
    joins(:sectors_studies).
    group("sectors.id").
    order("COUNT(sectors.id) DESC").
    limit(10)
  }
end

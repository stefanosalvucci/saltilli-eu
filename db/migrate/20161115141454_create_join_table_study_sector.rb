class CreateJoinTableStudySector < ActiveRecord::Migration
  def change
    create_join_table :Studies, :Sectors do |t|
      # t.index [:study_id, :sector_id]
      # t.index [:sector_id, :study_id]
    end
  end
end

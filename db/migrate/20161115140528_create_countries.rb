class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string :value
      t.string :url

      t.timestamps null: false
    end
  end
end

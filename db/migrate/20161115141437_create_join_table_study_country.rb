class CreateJoinTableStudyCountry < ActiveRecord::Migration
  def change
    create_join_table :Studies, :Countries do |t|
      # t.index [:study_id, :country_id]
      # t.index [:country_id, :study_id]
    end
  end
end

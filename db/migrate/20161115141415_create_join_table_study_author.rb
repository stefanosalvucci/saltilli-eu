class CreateJoinTableStudyAuthor < ActiveRecord::Migration
  def change
    create_join_table :Studies, :Authors do |t|
      # t.index [:study_id, :author_id]
      # t.index [:author_id, :study_id]
    end
  end
end

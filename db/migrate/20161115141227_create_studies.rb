class CreateStudies < ActiveRecord::Migration
  def change
    create_table :studies do |t|
      t.string :title
      t.integer :year
      t.text :description
      t.string :url

      t.timestamps null: false
    end
  end
end

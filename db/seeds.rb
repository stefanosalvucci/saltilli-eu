require 'json'

file = File.read('db/studies.txt')

data_hash = JSON.parse(file)

data_hash.each do |study|
  puts "==================== BEGIN STUDY ========================="
  new_study = Study.new
  new_study.title = study['title'][0]
  new_study.description = study['description'][0]
  new_study.url = study['url'][0]
  new_study.year = study['year'][0]


  if study["authors"][0]["list"].present?
    study["authors"][0]["list"].each do |author|
      a = Author.find_by_name(author['author'][0])
      if a.nil?
        a = Author.create(name: author['author'][0], url: author['url'][0])
      end
      new_study.authors << a
    end
  end

  if study["countries"][0]["list"].present?
    study["countries"][0]["list"].each do |country|
      c = Country.find_by_value(country['country'][0])
      if c.nil?
        c = Country.create(value: country['country'][0], url: country['url'][0])
      end
      new_study.countries << c
    end
  end

  if study["sectors"][0]["list"].present?
    study["sectors"][0]["list"].each do |sector|
      s = Sector.find_by_name(sector['sector'][0])
      if s.nil?
        s = Sector.create(name: sector['sector'][0], url: sector['url'][0])
      end
      new_study.sectors << s
    end
  end

  new_study.save

end
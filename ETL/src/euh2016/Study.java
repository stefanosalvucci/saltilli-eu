package euh2016;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.util.Pair;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Study
{
	// RowKey
	public String rk;
	
	// INFO
	public String title;
	public int year;
	public String description;
	public String url;
	
	// SECTORS
	public List<Pair<String, String>> sectors;

	// AUTHORS
	public List<Pair<String, String>> authors;

	// LINKS
	public List<Pair<String, String>> links;

	// COUNTRIES
	public List<Pair<String, String>> countries;

	public Study(String rk, String url)
	{
		this.rk = rk;
		this.url = url;

		Document document = null;
		try
		{
			document = Jsoup.parse(new URL(url), 5000);
		}
		catch (IOException e)
		{
			System.out.println("StudyPage creation error: " + e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}

		// Get the first 2 tables ('study_info' and 'study_data')
		Elements tables = document.select("table");
		Elements study_info = tables.get(0).select("tr");
		Elements study_data = tables.get(1).select("tr");

		// Remove first element (title of the table)
		study_info.remove(0);
		study_data.remove(0);

		this.title = study_info.get(0).select("td").get(1).text();
		this.description = study_data.get(0).select("td").get(1).text();
		try
		{
			this.year = Integer.parseInt(study_info.get(2).select("td").get(1).text());
		}
		catch(Exception e)
		{
			System.out.println("WARNING:\t" + this.rk + " has no have a 'Year' field yet");
		}
		
		/*
		 * 	Sectors
		 */
		this.sectors = new ArrayList<Pair<String, String>>();
		
		Elements e_sectors = study_data.get(5).select("td").get(1).children();
		for (Element sector : e_sectors)
		{
			Pair<String, String> p = new Pair<String, String>();
			p.setFirst(sector.text());
			p.setSecond(sector.select("a").attr("abs:href"));
			
			this.sectors.add(p);
		}
		
		/*
		 * 	Authors
		 */
		this.authors = new ArrayList<Pair<String, String>>();

		Elements e_authors = study_info.get(1).select("td").get(1).children();
		for (Element author : e_authors)
		{
			Pair<String, String> p = new Pair<String, String>();
			p.setFirst(author.text());
			p.setSecond(author.select("a").attr("abs:href"));
			
			this.authors.add(p);
		}
		
		/*
		 * 	Links
		 */
		this.links = new ArrayList<Pair<String, String>>();

		Elements e_link = study_info.get(4).select("td").get(1).children();
		for (Element link : e_link)
		{
			Pair<String, String> p = new Pair<String, String>();
			p.setFirst(link.text());
			p.setSecond(link.select("a").attr("abs:href"));
			
			this.links.add(p);
		}
		
		/*
		 * 	Countries
		 */
		this.countries = new ArrayList<Pair<String, String>>();

		Elements e_countries = study_data.get(6).select("td").get(1).children();
		for (Element country : e_countries)
		{
			Pair<String, String> p = new Pair<String, String>();
			p.setFirst(country.text());
			p.setSecond(country.select("a").attr("abs:href"));
			
			this.countries.add(p);
		}
		
	}

	public String explain()
	{
		String explain = "";
		explain += "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
		
		explain += "ROWKEY" + "\t\"" + this.rk + "\"\n\n";
		
		explain += "INFO" + "\n";
		explain += "\t" + "title" + "\n" + "\t\t" + this.title + "\n";
		explain += "\t" + "year" + "\n" + "\t\t" + this.year + "\n";
		explain += "\t" + "description" + "\n" + "\t\t" + this.description + "\n";
		explain += "\t" + "url" + "\n" + "\t\t" + this.url + "\n";
		
		explain += "SECTORS" + "\n";
		for(Pair<String, String> p : this.sectors)
		{
			explain += "\t" + p.getFirst() + "\n" + "\t\t" + p.getSecond() + "\n";
		}
		
		explain += "AUTHORS" + "\n";
		for(Pair<String, String> p : this.authors)
		{
			explain += "\t" + p.getFirst() + "\n" + "\t\t" + p.getSecond() + "\n";
		}
		
		explain += "LINKS" + "\n";
		for(Pair<String, String> p : this.links)
		{
			explain += "\t" + p.getFirst() + "\n" + "\t\t" + p.getSecond() + "\n";
		}
		
		explain += "COUNTRIES" + "\n";
		for(Pair<String, String> p : this.countries)
		{
			explain += "\t" + p.getFirst() + "\n" + "\t\t" + p.getSecond() + "\n";
		}
		
		return explain;
	}
	
	public Put producePut()
	{
		// Rowkey
		Put p = new Put(this.rk.getBytes());
		
		// INFO
		p.addColumn(
				Constant.INFO,
				Constant.TITLE,
				this.title.getBytes()
				);
		p.addColumn(
				Constant.INFO,
				Constant.YEAR,
				Bytes.toBytes(this.year)
				);
		p.addColumn(
				Constant.INFO,
				Constant.DESCRIPTION,
				this.description.getBytes()
				);
		p.addColumn(
				Constant.INFO,
				Constant.URL,
				this.url.getBytes()
				);
		
		// SECTORS
		for(Pair<String, String> pair : sectors)
		{
			p.addColumn(
					Constant.SECTORS,
					pair.getFirst().getBytes(),
					pair.getSecond().getBytes()
					);
		}
		
		// AUTHORS
		for(Pair<String, String> pair : authors)
		{
			p.addColumn(
					Constant.AUTHORS,
					pair.getFirst().getBytes(),
					pair.getSecond().getBytes()
					);
		}
		
		// LINKS
		for(Pair<String, String> pair : links)
		{
			p.addColumn(
					Constant.LINKS,
					pair.getFirst().getBytes(),
					pair.getSecond().getBytes()
					);
		}
		
		// COUNTRIES
		for(Pair<String, String> pair : countries)
		{
			p.addColumn(
					Constant.COUNTRIES,
					pair.getFirst().getBytes(),
					pair.getSecond().getBytes()
					);
		}
		
		return p;
	}
	
	public JSONObject toJson()
	{
		JSONObject jsonObject = new JSONObject();
		
		jsonObject.append("key", this.rk);
		jsonObject.append("title", this.title);
		jsonObject.append("year", this.year);
		jsonObject.append("description", this.description);
		jsonObject.append("url", this.url);
		
		// SECTORS
		JSONObject sectors = new JSONObject();
		for(Pair<String, String> pair : this.sectors)
		{
			JSONObject sector = new JSONObject();
			sector.append("sector", pair.getFirst());
			sector.append("url", pair.getSecond());
			sectors.append("list", sector);
		}
		jsonObject.append("sectors", sectors);
		
		// AUTHORS
		JSONObject authors = new JSONObject();
		for(Pair<String, String> pair : this.authors)
		{
			JSONObject author = new JSONObject();
			author.append("author", pair.getFirst());
			author.append("url", pair.getSecond());
			authors.append("list", author);
		}
		jsonObject.append("authors", authors);
		
		// LINKS
		JSONObject links = new JSONObject();
		for(Pair<String, String> pair : this.links)
		{
			JSONObject link = new JSONObject();
			link.append("link", pair.getFirst());
			link.append("url", pair.getSecond());
			links.append("list", link);
		}
		jsonObject.append("links", links);
		
		// COUNTRIES
		JSONObject countries = new JSONObject();
		for(Pair<String, String> pair : this.countries)
		{
			JSONObject country = new JSONObject();
			country.append("country", pair.getFirst());
			country.append("url", pair.getSecond());
			countries.append("list", country);
		}
		jsonObject.append("countries", countries);
		
		return jsonObject;
	}

}

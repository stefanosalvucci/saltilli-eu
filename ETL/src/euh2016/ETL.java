package euh2016;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;

public class ETL
{
	
	public static void main(String[] args) throws IOException
	{
		Long startTime = Calendar.getInstance().getTimeInMillis();

		populateStudyTable();
		populateSectorTable();
		
		Long endTime = Calendar.getInstance().getTimeInMillis();
		int secs = (int) ((endTime - startTime) / 1000);
		System.out.println("\n" + "Population ended in:" + "\n" + secs/60 + "\t" + "minutes" + "\n" + secs%60 + "\t" + "seconds");
	}
	
	public static void populateStudyTable() throws IOException
	{
		URL studies_url = new URL(Constant.STUDIES_URL);
		Document document = Jsoup.parse(studies_url, 5000);
		Element table = document.select("table").first();
		Elements rows = table.select("tr");

		System.out.println(rows.size() + " elements found!");
		
		// Configure HBase
		Configuration configuration = HBaseConfiguration.create();
		@SuppressWarnings("deprecation")
		HTable ht = new HTable(configuration , Constant.TABLE_STUDIES);

		for (int i = 1; i < rows.size(); i++)
		{
			if(i%25==0)
				System.out.println((int)(i*100/rows.size()) + "%");
			
		    Element row = rows.get(i);
		    Elements cols = row.select("td");
		    
		    String rk = cols.get(0).text();
		    String url = cols.get(0).select("a").attr("abs:href");
		    
		    Study s = new Study(rk, url);
		    
		    // Write on HBase table
		    ht.put(s.producePut());
		    
		}
		
		ht.close();
		
	}
	
	private static void populateSectorTable() throws IOException
	{
		// Configure HBase
		Configuration configuration = HBaseConfiguration.create();
		@SuppressWarnings("deprecation")
		HTable ht = new HTable(configuration , Constant.TABLE_SECTORS);
		Constant.initialize();

		List<String> list = new ArrayList<String>(Constant.SECTORS_LIST.keySet());
		java.util.Collections.sort(list);

		for(String key : list)
		{
			System.out.println("~~~~~~~~~~~~~~~~~~" + key + "~~~~~~~~~~~~~~~~~~");
			Put p = extractStudies(key, new URL(createUrl(key)));
			
		    // Write on HBase table
			ht.put(p);
			System.out.println("\n");
		}

		ht.close();
	}
	
	private static String createUrl(String key) throws UnsupportedEncodingException
	{
		String output = key.toLowerCase();
		output = URLEncoder.encode(output, "UTF-8");
		output = output.replace(" ", "+");
		return Constant.start + output + Constant.end;
	}

	public static Put extractStudies(String key, URL sectors_url) throws IOException
	{
		Put p = new Put(key.getBytes());
		
		Document document = Jsoup.parse(sectors_url, 5000);
		Elements table = null;
		try
		{
			table = document.select("table").get(0).select("tr");
		}
		catch (Exception e)
		{
			return p;
		}
		table.remove(0);

		System.out.println(table.size() + " elements.");

		for(Element e : table)
		{
			Element study = e.child(0);
			p.addColumn(
					Constant.STUDIES,
					study.text().getBytes(),
					study.select("a").attr("abs:href").getBytes()
					);
		}

		return p;
	}

}

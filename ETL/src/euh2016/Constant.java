package euh2016;

import java.util.HashMap;
import java.util.Map;

public class Constant
{
	/*
	 *	URLs 
	 */
	
	public final static String STUDIES_URL = "http://www.copyrightevidence.org/evidence-wiki/index.php/Special:Ask/-5B-5BCategory:Studies-5D-5D/-3FHas-20full-20citation%3DCitation/-3FLast-20editor-20is%3DLast-20Editor/-3FModification-20date%3DModified/-3FCreation-20date%3DCreated/format%3Dtable/limit%3D600/offset%3D0";
	public final static String SECTORS_URL = "http://www.copyrightevidence.org/evidence-wiki/index.php/Category:Industries";

	/*
	 * 	'euh2016:studies'
	 */
	
	public final static byte[] TABLE_STUDIES = "euh2016:studies".getBytes();
	
	public final static byte[] INFO = "INFO".getBytes();
	public final static byte[] TITLE = "title".getBytes();
	public final static byte[] YEAR = "year".getBytes();
	public final static byte[] DESCRIPTION = "description".getBytes();
	public final static byte[] URL = "url".getBytes();
	
	public final static byte[] SECTORS = "SECTORS".getBytes();
	public final static byte[] AUTHORS = "AUTHORS".getBytes();
	public final static byte[] LINKS = "LINKS".getBytes();
	public final static byte[] COUNTRIES = "COUNTRIES".getBytes();
	
	/*
	 * 	'euh2016:sectors'
	 */
	
	public final static byte[] TABLE_SECTORS = "euh2016:sectors".getBytes();
	
	public final static byte[] STUDIES = "STUDIES".getBytes();
	
	public final static Map<String, String> SECTORS_LIST = new HashMap<>();
	
	public static void initialize()
	{
		SECTORS_LIST.put("Advertising", "http://www.copyrightevidence.org/evidence-wiki/index.php/Advertising");
		SECTORS_LIST.put("Architectural", "http://www.copyrightevidence.org/evidence-wiki/index.php/Architectural");
		SECTORS_LIST.put("Computer consultancy", "http://www.copyrightevidence.org/evidence-wiki/index.php/Computer_consultancy");
		SECTORS_LIST.put("Computer programming", "http://www.copyrightevidence.org/evidence-wiki/index.php/Computer_programming");
		SECTORS_LIST.put("Creative, arts and entertainment", "http://www.copyrightevidence.org/evidence-wiki/index.php/Creative,_arts_and_entertainment");
		SECTORS_LIST.put("Cultural education", "http://www.copyrightevidence.org/evidence-wiki/index.php/Cultural_education");
		SECTORS_LIST.put("Film and motion pictures", "http://www.copyrightevidence.org/evidence-wiki/index.php/Film_and_motion_pictures");
		SECTORS_LIST.put("Photographic activities", "http://www.copyrightevidence.org/evidence-wiki/index.php/Photographic_activities");
		SECTORS_LIST.put("PR and communication", "http://www.copyrightevidence.org/evidence-wiki/index.php/PR_and_communication");
		SECTORS_LIST.put("Programming and broadcasting", "http://www.copyrightevidence.org/evidence-wiki/index.php/Programming_and_broadcasting");
		SECTORS_LIST.put("Publishing of books, periodicals and other publishing", "http://www.copyrightevidence.org/evidence-wiki/index.php/Publishing_of_books,_periodicals_and_other_publishing");
		SECTORS_LIST.put("Software publishing (including video games)", "http://www.copyrightevidence.org/evidence-wiki/index.php/Software_publishing_(including_video_games)");
		SECTORS_LIST.put("Sound recording and music publishing", "http://www.copyrightevidence.org/evidence-wiki/index.php/Sound_recording_and_music_publishing");
		SECTORS_LIST.put("Specialised design", "http://www.copyrightevidence.org/evidence-wiki/index.php/Specialised_design");
		SECTORS_LIST.put("Television programmes", "http://www.copyrightevidence.org/evidence-wiki/index.php/Television_programmes");
		SECTORS_LIST.put("Translation and interpretation", "http://www.copyrightevidence.org/evidence-wiki/index.php/Translation_and_interpretation");
	}
	
	public final static String start = "http://www.copyrightevidence.org/evidence-wiki/index.php?title=Special:Ask&offset=0&limit=500&q=%5B%5BHas+industry%3A%3A";
	public final static String end = "%5D%5D&p=format%3Dtable";

}

Rails.application.routes.draw do
  get '/botprivacy', to: 'application#botprivacy'
  mount Facebook::Messenger::Server, at: "bot"
end